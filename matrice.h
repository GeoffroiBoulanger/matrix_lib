#pragma once

#include <functional>
#include <stdexcept>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <memory>


class Matrice {
public:
	Matrice(unsigned short ligne, unsigned short colonne = 1) {
		this->nb_lignes = ligne;
		this->nb_colonnes = colonne;

		this->data = std::unique_ptr<float[]>(new float[ligne*colonne]);
		this->zeros();
	}

	Matrice(Matrice &autre) {
		this->nb_lignes = autre.nb_lignes;
		this->nb_colonnes = autre.nb_colonnes;

		this->data = std::unique_ptr<float[]>(new float[this->size()]);
		this->copy(autre);
	}

	float at(short index) { return this->data[index]; }
	float at(short ligne, short colonne) { return this->data[(ligne*this->nb_colonnes) + colonne]; }

	size_t size() { return (this->nb_lignes * this->nb_colonnes); }

	void zeros();
	void randomize();
	void map(std::function<float(float)>);
	void copy(Matrice&);

	Matrice operator*(Matrice &);
	Matrice operator*(float);
	bool operator==(Matrice &);

	short lignes() { return this->nb_lignes; }
	short colonnes() { return this->nb_colonnes; }

protected:
	float& ref_at(short index) { return this->data[index]; }
	float& ref_at(short ligne, short colonne) { return this->data[(ligne*this->nb_colonnes) + colonne]; }

private:
	float randomdec() { return (float(rand()) / 32767.0f); }
	
	unsigned short nb_lignes;
	unsigned short nb_colonnes;
	std::unique_ptr<float[]> data;
};

inline void Matrice::zeros() {
	for (unsigned i = 0; i < this->size(); i++)
		this->data[i] = 0.0f;
}

inline void Matrice::randomize() {
	static bool doit_seed = true;
	if (doit_seed) {
		srand(time(NULL));
		doit_seed = false;
	}

	for (size_t i = 0; i < this->size(); i++)
		this->data[i] = this->randomdec();
}

inline void Matrice::map(std::function<float(float)> modificateur) {
	for (unsigned i = 0; i < this->size(); i++)
		this->data[i] = modificateur(this->data[i]);
}

inline void Matrice::copy(Matrice& autre) {
	for (int i = 0; i < this->size(); i++)
		this->data[i] = autre.at(i);
}

inline Matrice Matrice::operator*(Matrice &Droite) {  // Aucune Gestion d'erreur -> ELSE
	if (this->nb_colonnes == Droite.nb_lignes) {
		Matrice Resultat(this->nb_lignes, Droite.nb_colonnes);  // = O

		for (unsigned short a = 0; a < Resultat.nb_lignes; a++)
			for (unsigned short b = 0; b < Resultat.nb_colonnes; b++)
				for (unsigned short c = 0; c < this->nb_colonnes; c++)
					Resultat.ref_at(a, b) += this->at(a, c) * Droite.at(c, b);

		return Resultat;

	}
	else if ((this->nb_lignes != Droite.nb_lignes) || (this->nb_colonnes != Droite.nb_colonnes)) {
		Matrice Resultat(this->nb_lignes, this->nb_colonnes);

		for (unsigned short i = 0; i < Resultat.nb_lignes; i++)
			for (unsigned short j = 0; j < Resultat.nb_colonnes; j++)
				Resultat.ref_at(i, j) = this->at(i, j) * Droite.at(i, j);

		return Resultat;
	}
}

inline Matrice Matrice::operator*(float scalaire) {
	Matrice mat_temp = *this;

	for (unsigned i = 0; i < mat_temp.size(); i++)
		mat_temp.data[i] *= scalaire;

	return mat_temp;
}

inline bool Matrice::operator==(Matrice &autre_matrice) {
	if ((this->nb_lignes != autre_matrice.nb_lignes) || (this->nb_colonnes != autre_matrice.nb_colonnes))
		return false;

	for (unsigned i = 0; i < this->size(); i++)
		if (this->data[i] != autre_matrice.data[i])
			return false;

	return true;
}
